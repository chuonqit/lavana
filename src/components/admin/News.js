import { createNews, deleteNewsByID, updateNews } from "../../api/news";

export const ListNews = {
    handleDelete: function () {
        const newsButtonElement = document.querySelectorAll("#listNews .btn-delete-news");
        newsButtonElement.forEach((element) => {
            element.onclick = async function () {
                let parentElement = element.parentElement,
                    id = parentElement.getAttribute("data-id");
                const response = await deleteNewsByID(id);
                if (response) {
                    alert("Delete successfully!");
                    parentElement.parentElement.remove();
                }
            };
        });
    },
    render: function (news) {
        return /* html */ `
            <table class="min-w-full divide-y divide-gray-200 border" id="listNews">
                <thead class="bg-gray-50">
                    <tr>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">NEWS</th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">DESCRIPTION</th>
                        <th scope="col" class="relative px-6 py-3">
                            <span class="sr-only">Action</span>
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    ${news
                        .map(
                            (item, key) => /* html */ `
                                <tr>
                                    <td class="px-6 py-4">
                                        <div class="flex items-center">
                                            <div class="flex-shrink-0 h-10 w-10">
                                                <img
                                                    class="h-10 w-10 rounded-full bg-gray-200"
                                                    src="${item.image}"
                                                    alt="${item.title}"
                                                />
                                            </div>
                                            <div class="ml-4">
                                                <div class="text-sm text-gray-900">${item.title}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="px-6 py-4">
                                        <div class="text-sm text-gray-900">${item.description}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap text-right" data-id="${item.id}">
                                        <a href="/admin/news/${item.id}/edit" class="bg-white border py-2 px-2 ml-3 inline-flex hover:bg-gray-100">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                            </svg>
                                        </a>
                                        <button class="btn-delete-news bg-white py-2 px-2 border ml-2 hover:bg-gray-100">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                            `
                        )
                        .join("")}
                </tbody>
            </table>
        `;
    },
};

export const CreateNews = {
    handleSubmit: function () {
        const formNews = document.querySelector("#formNews");
        formNews.onsubmit = async function (e) {
            e.preventDefault();
            const { image, title, description, content } = e.target;
            const params = {
                image: image.value,
                title: title.value,
                description: description.value,
                content: content.value,
            };
            const response = await createNews(params);
            if (response.status === 201) {
                alert("Create news successfully!");
                formNews.reset();
            } else {
                alert("Create news failed!");
            }
        };
    },
    render: function () {
        return /* html */ `
            <form id="formNews" class="border">
                <div class="py-3 px-4 text-left border-b font-medium text-gray-500 uppercase tracking-wider bg-gray-50">
                    Create News
                </div>
                <div class="p-3">
                    <div class="mb-3">
                        <input name="image" type="text" class="border px-3 py-1 w-full focus:outline-none" placeholder="Imgae (URL)" value="http://placeimg.com/640/480"/>
                    </div>
                    <div class="mb-3">
                        <input name="title" type="text" class="border px-3 py-1 w-full focus:outline-none" placeholder="Title"/>
                    </div>
                    <div class="mb-3">
                        <input name="description" type="text" class="border px-3 py-1 w-full focus:outline-none" placeholder="Description"/>
                    </div>
                    <div class="mb-3">
                        <textarea name="content" rows="5" class="border px-3 py-1 w-full focus:outline-none" placeholder="Content"></textarea>
                    </div>
                    <button type="submit" class="py-1 px-5 border hover:bg-gray-50">Save</button>
                </div>
            </form>
        `;
    },
};

export const UpdateNews = {
    handleSubmit: function () {
        const formNews = document.querySelector("#formNews");
        formNews.onsubmit = async function (e) {
            e.preventDefault();
            const { image, title, description, content, id } = e.target;
            const params = {
                image: image.value,
                title: title.value,
                description: description.value,
                content: content.value,
            };
            const response = await updateNews(id.value, params);
            if (response.status === 200) {
                alert("Update news successfully!");
            } else {
                alert("Update news failed!");
            }
        };
    },
    render: function (news) {
        return /* html */ `
            <form id="formNews" class="border">
                <div class="py-3 px-4 text-left border-b font-medium text-gray-500 uppercase tracking-wider bg-gray-50">
                    UPDATE: ${news.title}
                </div>
                <div class="p-3">
                    <input name="id" value="${news.id}" type="hidden"/>
                    <div class="mb-3">
                        <input name="image" value="${news.image}" type="text" class="border px-3 py-1 w-full focus:outline-none" placeholder="Imgae (URL)" value="http://placeimg.com/640/480"/>
                    </div>
                    <div class="mb-3">
                        <input name="title" value="${news.title}" type="text" class="border px-3 py-1 w-full focus:outline-none" placeholder="Title"/>
                    </div>
                    <div class="mb-3">
                        <input name="description" value="${news.description}" type="text" class="border px-3 py-1 w-full focus:outline-none" placeholder="Description"/>
                    </div>
                    <div class="mb-3">
                        <textarea name="content" rows="5" class="border px-3 py-1 w-full focus:outline-none" placeholder="Content">${news.content}</textarea>
                    </div>
                    <button type="submit" class="py-1 px-5 border hover:bg-gray-50">Save</button>
                </div>
            </form>
        `;
    },
};
