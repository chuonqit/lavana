export default function AdminLayout(Content, Element) {
    Element.innerHTML = /* html */ `
        <div class="flex text-[14px] flex-col md:flex-row">
            <div class="w-64 bg-white h-screen md:border-r-2 p-2 duration-300 hidden md:block" id="sidebarMenu">
                <a href="/admin/dashboard" class="flex items-center h-[40px] px-3 w-full hover:bg-gray-100 hover:border-l-2 border-l-2 hover:border-black duration-200">Dashboard</a>
                <a href="/admin/news" class="flex items-center h-[40px] px-3 w-full hover:bg-gray-100 hover:border-l-2 border-l-2 hover:border-black duration-200">News</a>
            </div>
            <div class="w-64 -ml-64 top-0 fixed z-[12] bg-white h-screen md:border-r-2 p-2 duration-300 shadow md:hidden" id="sidebarMenuMobile">
                <a href="/admin/dashboard" class="flex items-center h-[40px] px-3 w-full hover:bg-gray-100">Dashboard</a>
                <a href="/admin/news" class="flex items-center h-[40px] px-3 w-full hover:bg-gray-100">News</a>
            </div>
            <div class="w-screen h-screen">
                <div class="md:static w-full z-[11] fixed top-0 left-0 flex items-center h-[50px] border-b-2 bg-white pr-4">
                    <button class="hidden ml-4 bg-white md:flex items-center h-[35px] px-2 border hover:bg-gray-100" id="toggleSidebar">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path
                                fill-rule="evenodd"
                                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                                clip-rule="evenodd"
                            />
                        </svg>
                    </button>
                    
                    <button class="ml-4 bg-white md:hidden duration-300 flex items-center h-[35px] px-2 border hover:bg-gray-100" id="toggleSidebarMobile">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path
                                fill-rule="evenodd"
                                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                                clip-rule="evenodd"
                            />
                        </svg>
                    </button>

                    <div class="dropdown ml-auto relative">
                        <button class="flex items-center h-[35px] px-3 border hover:bg-gray-100 dropdown-btn">Users</button>
                        <div class="hidden dropdown-menu absolute top-[40px] right-0 min-w-[200px] min-h-[50px] py-2 px-3 bg-white border">
                            <a href="#" class="flex items-center p-2 hover:bg-gray-100 border-b">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                </svg>
                                User profile
                            </a>
                            <a href="#" class="flex items-center p-2 hover:bg-gray-100 border-b">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                                    />
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                                Settings
                            </a>
                            <a href="#" class="flex items-center p-2 hover:bg-gray-100">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
                                    />
                                </svg>
                                Logout
                            </a>
                        </div>
                    </div>
                </div>
                <div class="py-3 px-4 md:mt-0 mt-[50px] z-[10]">
                    ${Content}
                </div>
            </div>
        </div>
    `;

    let isToggle = false;
    let isToggleMobile = false;
    const btnToggle = document.querySelector("#toggleSidebar");
    const btnToggleMobile = document.querySelector("#toggleSidebarMobile");
    const sidebar = document.querySelector("#sidebarMenu");
    const sidebarMobile = document.querySelector("#sidebarMenuMobile");
    const sidebarLink = sidebar.querySelectorAll("a");
    const sidebarLinkMobile = sidebarMobile.querySelectorAll("a");
    sidebarLink.forEach((link) => {
        let hrefValue = link.getAttribute("href");
        let linkPage = window.location.pathname;
        if (linkPage.includes(hrefValue)) {
            link.classList.add("border-l-2", "border-black", "bg-gray-100");
        }
    });
    sidebarLinkMobile.forEach((link) => {
        let hrefValue = link.getAttribute("href");
        let linkPage = window.location.pathname;
        if (linkPage.includes(hrefValue)) {
            link.classList.add("border-l-2", "border-black", "bg-gray-100");
        }
    });
    btnToggle.onclick = function () {
        isToggle = !isToggle;
        sidebar.classList.toggle("-ml-64", isToggle);
    };
    btnToggleMobile.onclick = function () {
        isToggleMobile = !isToggleMobile;
        sidebarMobile.classList.toggle("-ml-64", !isToggleMobile);
        btnToggleMobile.classList.toggle("ml-64", isToggleMobile);
        btnToggleMobile.classList.toggle("border-l-0", isToggleMobile);
    };

    const dropdown = document.querySelectorAll(".dropdown");
    for (let elem of dropdown) {
        let dropdownButton = elem.querySelector(".dropdown-btn");
        let dropdownMenu = elem.querySelector(".dropdown-menu");
        dropdownButton.addEventListener("click", function (event) {
            if (dropdownMenu.classList.contains("hidden")) {
                dropdownMenu.classList.remove("hidden");
                return;
            }
            dropdownMenu.classList.add("hidden");
        });
    }
    document.addEventListener("click", function (event) {
        const menuOpen = document.querySelectorAll(".dropdown-menu");
        for (let item of menuOpen) {
            let dropdownButton = item.parentNode.querySelector(".dropdown-btn");
            if (!item.contains(event.target) && !dropdownButton.contains(event.target)) {
                item.classList.add("hidden");
            }
        }
    });
}
