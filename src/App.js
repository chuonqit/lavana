import Navigo from "navigo";
import { Render } from "./utils";

import Home from "./pages/Home";
import Dashboard from "./pages/admin/Dashboard";

import NotFound from "./pages/errors/NotFound";
import Login from "./pages/auth/Login";
import Register from "./pages/auth/Register";
import NewsCreate from "./pages/admin/news/NewsCreate";
import NewsUpdate from "./pages/admin/news/NewsUpdate";
import NewsList from "./pages/admin/news/NewsList";

export default function App() {
    const Route = new Navigo("/", { linksSelector: "a" });

    Render.root("root");
    Route.on({
        // client router
        "/": () => Render.client(Home),

        // auth router
        "/auth/login": () => Render.none(Login),
        "/auth/register": () => Render.none(Register),

        // admin router
        "/admin/dashboard": () => Render.admin(Dashboard),
        "/admin/news": () => Render.admin(NewsList),
        "/admin/news/create": () => Render.admin(NewsCreate),
        "/admin/news/:id/edit": ({ data }) => Render.admin(NewsUpdate, data),
    });

    Route.notFound(() => Render.none(NotFound));

    Route.resolve();
}
