import Axios from "./instance";

export const getAllNews = async () => {
    try {
        const { data } = await Axios.get("/news");
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const getNewsByID = async (id) => {
    try {
        const { data } = await Axios.get("/news");
        return data.find((item) => item.id == id);
    } catch (error) {
        console.error(error);
    }
};

export const deleteNewsByID = async (id) => {
    try {
        const { data } = await Axios.delete(`/news/${id}`);
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const createNews = async (params) => {
    try {
        const response = await Axios.post("/news", params);
        return response;
    } catch (error) {
        console.error(error);
    }
};

export const updateNews = async (id, params) => {
    try {
        const response = await Axios.put(`/news/${id}`, params);
        return response;
    } catch (error) {
        console.error(error);
    }
};
