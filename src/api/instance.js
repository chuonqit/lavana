import axios from "axios";

const Axios = axios.create({
    // baseURL: "http://localhost:3001/",
    baseURL: "https://61e7a792e32cd90017acbbd3.mockapi.io/api/v1/",
});

axios.defaults.headers.post["Content-Type"] = "application/json";

export default Axios;
