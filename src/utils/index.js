import AdminLayout from "../containers/admin/AdminLayout";
import ClientLayout from "../containers/client/ClientLayout";

export const Render = {
    rootElement: document.getElementById("root"),
    admin: async function (Component, params) {
        await Component.beforeRender(params);
        AdminLayout(await Component.render(params), this.rootElement);
        await Component.afterRender(params);
    },
    client: async function (Component, params) {
        await Component.beforeRender(params);
        ClientLayout(await Component.render(params), this.rootElement);
        await Component.afterRender(params);
    },
    none: async function (Component, params) {
        await Component.beforeRender(params);
        this.rootElement.innerHTML = await Component.render(params);
        await Component.afterRender(params);
    },
    root: function (rootName) {
        this.rootElement = document.getElementById(rootName);
    },
};

export const setTitle = (title) => {
    document.title = title;
};
