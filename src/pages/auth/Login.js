import { setTitle } from "../../utils";

const Login = {
    beforeRender: function () {
        setTitle("Login Page");
    },
    afterRender: function () {},
    render: function () {
        return /* html */ `
            <ul class="flex gap-4">
                <li><a href="/" class="px-3 py-2 rounded bg-gray-200 hover:bg-gray-300 duration-300 block my-3">Home</a></li>
                <li><a href="/auth/login" class="px-3 py-2 rounded bg-gray-200 hover:bg-gray-300 duration-300 block my-3">Login</a></li>
                <li><a href="/auth/register" class="px-3 py-2 rounded bg-gray-200 hover:bg-gray-300 duration-300 block my-3">Register</a></li>
                <li><a href="/admin/dashboard" class="px-3 py-2 rounded bg-gray-200 hover:bg-gray-300 duration-300 block my-3">Dashboard</a></li>
            </ul>
            <h1>Login Page</h1>
        `;
    },
};

export default Login;
