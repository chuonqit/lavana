import { getAllNews } from "../../api/news";
import { ListNews } from "../../components/admin/News";
import { setTitle } from "../../utils";

const Dashboard = {
    state: {
        news: [],
    },
    beforeRender: async function () {},
    afterRender: function () {
        setTitle("Dashboard Page");
    },
    render: function () {
        return /* html */ `
            <h1>Dashboard Page</h1>
        `;
    },
};

export default Dashboard;
