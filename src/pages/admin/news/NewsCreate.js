import { setTitle } from "../../../utils";
import { CreateNews } from "../../../components/admin/News";

const NewsCreate = {
    beforeRender: function () {},
    afterRender: function () {
        setTitle("Create News");
        CreateNews.handleSubmit();
    },
    render: function () {
        return /* html */ `
            <a href="/admin/news" class="py-1 px-5 border hover:bg-gray-50 mb-3 inline-block">List News</a>
            ${CreateNews.render()}
        `;
    },
};

export default NewsCreate;
