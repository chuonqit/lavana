import { getAllNews } from "../../../api/news";
import { ListNews } from "../../../components/admin/News";
import { setTitle } from "../../../utils";

const NewsList = {
    state: {
        news: [],
    },
    beforeRender: async function () {
        this.state.news = await getAllNews();
    },
    afterRender: function () {
        setTitle("Dashboard Page");
        ListNews.handleDelete();
    },
    render: function () {
        return /* html */ `
            <a href="/admin/news/create" class="py-1 px-5 border hover:bg-gray-50 mb-3 inline-block">Create News</a>
            ${ListNews.render(this.state.news)}
        `;
    },
};

export default NewsList;
