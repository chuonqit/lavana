import { setTitle } from "../../../utils";
import { UpdateNews } from "../../../components/admin/News";
import { getNewsByID } from "../../../api/news";

const NewsUpdate = {
    state: {
        news: {},
    },
    beforeRender: async function ({ id }) {
        this.state.news = await getNewsByID(id);
    },
    afterRender: function () {
        setTitle(`Update ${this.state.news.title}`);
        UpdateNews.handleSubmit();
    },
    render: function () {
        return /* html */ `
            <a href="/admin/news" class="py-1 px-5 border hover:bg-gray-50 mb-3 inline-block">List News</a>
            ${UpdateNews.render(this.state.news)}
        `;
    },
};

export default NewsUpdate;
