import { setTitle } from "../utils";

const Home = {
    beforeRender: async function () {},
    afterRender: function () {
        setTitle("Home Page");
    },
    render: function () {
        return /* html */ `
            <h1>Home Page</h1>
        `;
    },
};

export default Home;
